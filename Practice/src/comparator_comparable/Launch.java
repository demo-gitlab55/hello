package comparator_comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Launch 
{
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		ArrayList<Laptop> al=new ArrayList<Laptop>();
		al.add(new Laptop("Dell",16,10000));
		al.add(new Laptop("Apple",12,30000));
		al.add(new Laptop("Lenovo",8,20000));
		
		Comparator<Laptop> con=new Comparator<Laptop>()
		{
			@Override
			public int compare(Laptop l1, Laptop l2) 
			{
				if(l1.getPrice()<l2.getPrice())
					return 1;
				else
					return -1;
			}
		};
		Collections.sort(al,con);
		
		for(Laptop l:al)
		{
			System.out.println(l);
		}
		

	}

}
