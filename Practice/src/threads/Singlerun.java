package threads;

import java.util.Scanner;

public class Singlerun {

	public static void main(String[] args) throws InterruptedException 
	{
		Dummy d=new Dummy();
		Thread t1=new Thread(d);
		Thread t2=new Thread(d);
		Thread t3=new Thread(d);
		t1.setName("BANK");
		t2.setName("NUM");
		t3.setName("CHAR");
		t1.start();
//		t1.join();
		t2.start();
	//	t2.join();
		t3.start();
	//	t3.join();
	}

}
class Dummy implements Runnable
{
	public void run()
	{
		Thread t=Thread.currentThread();
		String name=t.getName();
		if(name.equals("BANK"))
		{
			banking();
		}
		else if(name.equals("NUM"))
		{
			numPrinting();
		}
		else
		{
			charPrinting();
		}
	}
	public void banking()
	{
		System.out.println("Banking process started");
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter bank account no");
		int bankno=scan.nextInt();
		System.out.println("Enter password");
		int password=scan.nextInt();
		try 
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e) 
		{	
			e.printStackTrace();
		}
		System.out.println("collect money");
		System.out.println("Banking activity finished");
	}
	public void numPrinting()
	{
		System.out.println("Num Printing started");
		for(int i=0;i<5;i++)
		{
			System.out.println(i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Num printing ended");
	}
	public void charPrinting()
	{
		System.out.println("char Printing started");
		for(int i=0;i<5;i++)
		{
			System.out.println((char)(i+65));
			try 
			{
				Thread.sleep(1000);
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("char printing ended");
	}
}