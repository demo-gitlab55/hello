package threads;

public class Threadassignment2 {
	public static void main(String[] args) {

		PrintMethod p = new PrintMethod();
		Demoo d = new Demoo(p);
		Thread t1 = new Thread(d);
		Thread5 t2 = new Thread5(p);
		t1.start();
		t2.start();
	}
}

class PrintMethod 
{
	boolean valueset = false;
	synchronized void print1(int i) 
	{
		while (valueset) 
		{
			try
			{
				wait();
			} 
			catch (Exception e) 
			{
			}
		}
		System.out.println(i);
		valueset = true;
		notify();
	}

	synchronized void print10(int i) 
	{
		while (!valueset) 
		{
			try 
			{
				wait();
			}
			catch (Exception e) {
			}
		}
		System.out.println(i);
		valueset = false;
		notify();
	}
}

class Demoo implements Runnable 
{
	PrintMethod a;

	public Demoo(PrintMethod p) 
	{
		a = p;
	}

	public void run() 
	{
		for (int i = 10; i >= 6; i--) 
		{
			a.print10(i);
			try 
			{
				Thread.sleep(500);
			} 
			catch (Exception e) 
			{
			}
		}

	}
}

class Thread5 extends Thread 
{
	PrintMethod a;
	public Thread5(PrintMethod p) 
	{
		a = p;
	}

	public void run() 
	{
		for (int i = 1; i <= 5; i++) 
		{
			a.print1(i);
			try 
			{
				Thread.sleep(500);
			}
			catch (Exception e) 
			{
			}
		}
	}
}