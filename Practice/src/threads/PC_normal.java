package threads;

public class PC_normal 
{
	public static void main(String[] args) 
	{
		Queue q=new Queue();
		Producer p=new Producer(q);
		Consumer c=new Consumer(q);
		p.start();
		c.start();

	}

}
class Queue
{
	int x;
	public void put(int i)
	{
		x=i;
		System.out.println("I have put into x"+" "+x);
	}
	public void get()
	{
		System.out.println("I have taken from x"+" "+x);
	}
}
class Producer extends Thread
{
	Queue a;
	public Producer(Queue m)
	{
		a=m;
	}
	public void run()
	{
		int j=0;
		while(true)
		{
			a.put(j++);
		}
		
	}
}
class Consumer extends Thread
{
	Queue b;
	public Consumer(Queue n)
	{
		b=n;
	}
	public void run()
	{
		while(true)
		{
			b.get();
		}
		
	}
}