package threads;

import java.util.Scanner;

public class DaemonUse {

	public static void main(String[] args) throws InterruptedException 
	{
		Msword ms1=new Msword();
		Msword ms2=new Msword();
		Msword ms3=new Msword();
		ms1.setName("TYPE");
		ms2.setName("SPELL");
		ms3.setName("SAVE");
		ms2.setDaemon(true);
		ms3.setDaemon(true);
//		ms2.setPriority(3);
	//	ms2.setPriority(3);
		ms2.start();
		ms1.start();
		ms1.join();
		ms3.start();

	}

}
class Msword extends Thread
{
	public void run()
	{
		Thread t=Thread.currentThread();
		String name=t.getName();
		if(name.equals("TYPE"))
		{
			try {
				typing();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(name.equals("SPELL"))
		{
			spellCheck();
		}
		else
		{
			autoSaving();
		}
	}
	public void typing() throws InterruptedException
	{
		for(int i=0;i<5;i++)
		{
			System.out.println("typing ..");
			Thread.sleep(1000);
		}
	}
	public void spellCheck()
	{
		for(;true;)
		{
			System.out.println("spell checking");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public void autoSaving()
	{
		for(;true;)
		{
			System.out.println("auto saving");
			try 
			{
				Thread.sleep(1000);
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}