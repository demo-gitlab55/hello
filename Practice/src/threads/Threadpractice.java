package threads;

public class Threadpractice 
{

	public static void main(String[] args) 
	{	
		demo1 obj=new demo1();
		demo2 obj2=new demo2();
		obj.start();
		obj2.start();
	}

}
class demo1 extends Thread
{
	public void run()
	{
		for(int i=0;i<5;i++)
		{
		System.out.println("Hello");
		try 
		{
			Thread.sleep(500);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		}
	}
}
class demo2 extends Thread
{
	public void run()
	{
		for(int i=0;i<5;i++)
		{
		System.out.println(i);
		try 
		{
			Thread.sleep(500);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		}
	}
}