package threads;

import java.util.Scanner;

public class RunnableApproach {

	public static void main(String[] args) 
	{
		Demo4 d4=new Demo4();
		Demo5 d5=new Demo5();
		Demo6 d6=new Demo6();
		Thread t1=new Thread(d4);
		Thread t2=new Thread(d5);
		Thread t3=new Thread(d6);
		t1.start();
		t2.start();
		t3.start();

	}

}
class Demo4 implements Runnable
{
	public void run()
	{
		System.out.println("Banking process started");
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter bank account no");
		int bankno=scan.nextInt();
		System.out.println("Enter password");
		int password=scan.nextInt();
		try 
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e) 
		{	
			e.printStackTrace();
		}
		System.out.println("collect money");
		System.out.println("Banking activity finished");
	}
}
class Demo5 implements Runnable
{
	public void run()
	{
		System.out.println("Num Printing started");
		for(int i=0;i<5;i++)
		{
			System.out.println(i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Num printing ended");
	}
}
class Demo6 implements Runnable
{
	public void run()
	{
		System.out.println("char Printing started");
		for(int i=0;i<5;i++)
		{
			System.out.println((char)(i+65));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("char printing ended");
	}
}