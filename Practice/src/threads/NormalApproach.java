package threads;

import java.util.Scanner;

public class NormalApproach {

	public static void main(String[] args)
	{
		Demo d=new Demo();
		try {
			d.banking();
			d.numPrinting();
			d.charPrinting();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
class Demo
{
	public void banking() throws InterruptedException
	{
		System.out.println("Banking process started");
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter bank account no");
		int bankno=scan.nextInt();
		System.out.println("Enter password");
		int password=scan.nextInt();
		Thread.sleep(1000);
		System.out.println("collect money");
		System.out.println("Banking activity finished");
	}
	public void numPrinting() throws InterruptedException
	{
		System.out.println("Num Printing started");
		for(int i=0;i<5;i++)
		{
			System.out.println(i);
			Thread.sleep(1000);
		}
		System.out.println("Num printing ended");
		
	}
	public void charPrinting() throws InterruptedException
	{
		System.out.println("char Printing started");
		for(int i=0;i<5;i++)
		{
			System.out.println((char)(i+65));
			Thread.sleep(1000);
		}
		System.out.println("char printing ended");
	}
}
