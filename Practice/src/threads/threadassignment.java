package threads;

public class threadassignment 
{
	public static void main(String[] args) 
	{
		Print q = new Print();
		new Demo11(q);
		Thread t1=new Thread(new Demo10(q));
	}
}

class Print 
{
	boolean valueSet = false;
	public synchronized void print1(int i2) {
		while (valueSet) 
		{
			try {wait();} catch (Exception e) {}
		}
		System.out.println(i2);
		valueSet = true;
		notify();
	}

	public synchronized void print10(int i) {
		while (!valueSet) 
		{
			try {		wait();} catch (Exception e) {}
		}
		System.out.println(i);
		valueSet = false;
		notify();
	}
}

class Demo11 implements Runnable 
{
	Print q;
    int i;
	public Demo11(Print q) 
	{
		this.q = q;
		Thread t = new Thread(this, "Print1");
		t.start();
	}

	public void run()
	{
	for (int i = 0; i <= 5; i++)
	{
		q.print1(i);
		try{Thread.sleep(500);}catch(Exception e) {}
	}
	}
}

class Demo10 extends Thread 
{
	Print q;
	int i;
	public Demo10(Print q) 
	{
		this.q = q;
		Thread t = new Thread(this, "print10");
		t.start();
	}

	public void run() 
	{
		for (int i = 10; i >= 6; i--) 
		{
			q.print10(i);
			try
			{
				Thread.sleep(1000);
			}
			catch(Exception e) {}
		}
	}
}
