package collections;

import java.util.HashSet;
import java.util.Set;

public class CollectionsExample 
{
	public static void main(String[] args) 
	{
	Set<Employee> employeeSet=new HashSet<Employee>();
	Employee s1=new Employee(1,"surya",77);
	Employee s2=new Employee(1,"surya",77);
	Employee s3=new Employee(2,"ravi",27);
	Employee s4=new Employee(2,"ravi",27);
	Employee s5=new Employee(3,"vasavi",47);
	Employee s6=new Employee(4,"navya",57);
	Employee s7=new Employee(5,"alok",87);
	
	employeeSet.add(s1);
	employeeSet.add(s2);
	employeeSet.add(s3);
	employeeSet.add(s4);
	employeeSet.add(s5);
	employeeSet.add(s6);
	employeeSet.add(s7);
	for(Employee data:employeeSet)
	{
		System.out.println(data);
	}
	}

}
