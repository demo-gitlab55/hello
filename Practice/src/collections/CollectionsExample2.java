package collections;

import java.util.Set;
import java.util.TreeSet;

public class CollectionsExample2 {

	public static void main(String[] args) 
	{
		Set<Student> studentSet=new TreeSet<Student>();
		Student s1=new Student(1,"surya",77);
		Student s2=new Student(1,"surya",77);
		Student s3=new Student(2,"ravi",27);
		Student s4=new Student(2,"ravi",27);
		Student s5=new Student(3,"vasavi",47);
		Student s6=new Student(4,"navya",57);
		Student s7=new Student(5,"alok",87);
		
		studentSet.add(s1);
		studentSet.add(s2);
		studentSet.add(s3);
		studentSet.add(s4);
		studentSet.add(s5);
		studentSet.add(s6);
		studentSet.add(s7);
		for(Student data:studentSet)
		{
			System.out.println(data);
		}
		}
	

}
