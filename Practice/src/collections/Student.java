package collections;

class Student implements Comparable<Student>
{
	public int id;
	public String name;
	public int marks;
	public Student()
	{
		
	}
	
	public Student(int id, String name, int marks) {
		super();
		this.id = id;
		this.name = name;
		this.marks = marks;
	}

	public int getId() 
	{
		return id;
	}
	public void setId(int id) 
	{
		this.id = id;
	}
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	public int getMarks() 
	{
		return marks;
	}
	public void setMarks(int marks) 
	{
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", marks=" + marks + "]";
	}

	@Override
	public int compareTo(Student o) 
	{
		if(this.getMarks()==o.getMarks())
			return 0;
		if(this.getMarks()>o.getMarks())
			return 1;
		else
			return -1;
	}

//	@Override
//	public int hashCode() {
//		final int prime = 1;
//		int result = 1;
//		result = prime * result + id;
//		result = prime * result + marks;
//		result = prime * result + ((name == null) ? 0 : name.hashCode());
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Student other = (Student) obj;
//		if (id != other.id)
//			return false;
//		if (marks != other.marks)
//			return false;
//		if (name == null) {
//			if (other.name != null)
//				return false;
//		} else if (!name.equals(other.name))
//			return false;
//		return true;
//	}

	
}