package collections;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetExample 
{
	public static void main(String args[])
	{  
		  //sorted order
	    HashSet<String> set=new HashSet<String>();  
	    set.add("One");    
	    set.add("Two");    
	    set.add("Three");   
	    set.add("Four");  
	    set.add("One");  
	    Iterator<String> i=set.iterator();  
	    while(i.hasNext())  
	    {  
	        System.out.println(i.next());  
	    }  
	 }  
}
