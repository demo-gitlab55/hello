package amar_problems;

public class Splitmethod 
{
	public static void main(String[]args)
	{
		String s="More Practice will make me stronger";
		String[] ar=split(s);
		for(String data:ar)
		{
			words(data);
		}
	}
	private static String[] split(String s)
	{
		int sc=0,wc=0;
		for(int i=0;i<s.length();i++)
		{
			if(s.charAt(i)==' ')
			{
				sc++;
			}
		}
		wc=sc+1;
		int j=0;
		String ar[]=new String[wc];
		String temp="";
		for(int i=0;i<s.length();i++)
		{
			if(s.charAt(i)==' ')
			{
				ar[j]=temp;
				j++;
				temp="";
			}
			else
			{
				temp=temp+s.charAt(i);
			}
		}
		ar[j]=temp;
		return ar;
	}
	private static void words(String s)
	{
		char a=s.charAt(0);
		char b=s.charAt(s.length()-1);
		System.out.print(a+""+b+" ");
	}
}
