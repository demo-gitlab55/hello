package others;

public class LambdaDemo {

	public static void main(String[] args) 
	{
		A obj;
//		obj=new A() 
//		{
//			public void show()
//			{
//				System.out.println("hello");
//			}
//		};
		obj=() -> System.out.println("huhu");
		obj.show();
	}
	interface A
	{
		void show();
	}

}

//class demo implements A
//{
//
//	@Override
//	public void show() 
//	{
//		System.out.println("hi");
//	}
//	
//}