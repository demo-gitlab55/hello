package others;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StringToLowerCase 
{
	public static void main(String[] args) 
	{
		List<String> list=new ArrayList<>();
		list.add("A");
		list.add("B");
		list.add("C");
		System.out.println(list);
		
		List<String> list2=list.stream().map(s->s.toLowerCase()).collect(Collectors.toList());
		System.out.println(list2);
	}

}
