package others;

import java.util.function.BiConsumer;

public class LambdaExceptionHandling {

	public static void main(String[] args) {
		int[] num= {2,3,4,5};
		int key=0;
		process(num,key,wrapperLambda((v,k)->System.out.println(v/k)));

	}

	private static void process(int[] num, int key,BiConsumer<Integer,Integer> consumer) 
	{
		for(int i:num)
		{
			consumer.accept(i,key);
		}
		
	}
	
	private static BiConsumer<Integer,Integer> wrapperLambda(BiConsumer<Integer,Integer> consumer)
	{
		return (v,k) -> {
			try {
			consumer.accept(v, k);
			}
			catch(Exception e)
			{
				System.out.println("exception caught in wrapper lambda");
			}
		};
	}

}

