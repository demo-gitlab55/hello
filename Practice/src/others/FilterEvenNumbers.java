package others;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class FilterEvenNumbers {

	public static void main(String[] args) 
	{
		List<Integer> list=new ArrayList<>();
		for(int i=0;i<=10;i++)
		{
			list.add(i);
		}
//		List<Integer> list2=new ArrayList<>();
//		for(int l:list)
//		{
//			if(l%2==0)
//			{
//				list2.add(l);
//			}
//		}
		Comparator<Integer> comp=(i1,i2)->(i1).compareTo(i2);
		List<Integer> list2=list.stream().filter(i->i%2==0).collect(Collectors.toList());
		System.out.println(list2);
		
		Long count=list.stream().filter(i->i%2==0).count();
		System.out.println(count);
		
		List<Integer> list3 = list.stream().sorted().collect(Collectors.toList());
		System.out.println(list3);
		
		long max=list.stream().max(comp).get();
		System.out.println(max);
		
		long min=list.stream().min(comp).get();
		System.out.println(min);
	}

}
