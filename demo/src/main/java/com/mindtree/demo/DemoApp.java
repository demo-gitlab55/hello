package com.mindtree.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.mindtree.entity.Employee;

public class DemoApp {

	public static void main(String[] args) {
		
		Employee e=new Employee();
		e.setId(1);
		e.setName("surya");
		e.setSalary(20000);
		e.setDesignation("engineer");
		
		Configuration con=new Configuration().configure().addAnnotatedClass(Employee.class);
		SessionFactory sf=con.buildSessionFactory();
		Session session=sf.openSession();
		
		Transaction tx=session.beginTransaction();
		session.save(e);
		
		tx.commit();
		

	}

}
